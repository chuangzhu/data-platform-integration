﻿/*
Deployment script for blk-staging

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "blk-staging"
:setvar DefaultFilePrefix "blk-staging"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367)) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[Banner]...';


GO
CREATE TABLE [dbo].[Banner] (
    [BannerCode]       VARCHAR (50)  NOT NULL,
    [BannerName]       VARCHAR (100) NULL,
    [ParentBannerCode] VARCHAR (50)  NULL,
    [ParentBannerName] VARCHAR (100) NULL,
    CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED ([BannerCode] ASC)
);


GO
PRINT N'Creating [dbo].[Banner].[IX_Banner]...';


GO
CREATE NONCLUSTERED INDEX [IX_Banner]
    ON [dbo].[Banner]([BannerName] ASC);


GO
PRINT N'Creating [dbo].[BranchPlant]...';


GO
CREATE TABLE [dbo].[BranchPlant] (
    [BranchPlant]     VARCHAR (50)   NULL,
    [BranchPlantName] VARCHAR (100)  NULL,
    [Company]         NVARCHAR (255) NULL
);


GO
PRINT N'Creating [dbo].[Consignment]...';


GO
CREATE TABLE [dbo].[Consignment] (
    [ConsignmentNo]         VARCHAR (50)  NOT NULL,
    [ConsignmentItems]      VARCHAR (255) NULL,
    [ItemCount]             INT           NULL,
    [PostCode]              VARCHAR (25)  NULL,
    [ServiceProvideDepot]   VARCHAR (50)  NULL,
    [Status]                VARCHAR (50)  NULL,
    [StatusAvilabilityDate] DATETIME      NULL,
    [StatusCaptureDate]     DATETIME      NULL,
    [Suburb]                NVARCHAR (50) NULL,
    CONSTRAINT [PK_Consignment] PRIMARY KEY CLUSTERED ([ConsignmentNo] ASC)
);


GO
PRINT N'Creating [dbo].[Customer]...';


GO
CREATE TABLE [dbo].[Customer] (
    [BannerCode]     INT            NULL,
    [BusinessUnitId] VARCHAR (50)   NULL,
    [CatCode09]      VARCHAR (50)   NULL,
    [CatCode20]      VARCHAR (50)   NULL,
    [Channel]        VARCHAR (100)  NULL,
    [City]           VARCHAR (100)  NULL,
    [CustomerId]     VARCHAR (50)   NOT NULL,
    [CustomerName]   NVARCHAR (255) NOT NULL,
    [DemandGroup]    VARCHAR (50)   NULL,
    [PostCode]       VARCHAR (50)   NULL,
    [SalesUnit]      VARCHAR (50)   NULL,
    [SellingRegion]  VARCHAR (50)   NULL,
    [ShopperChannel] VARCHAR (50)   NULL,
    [State]          VARCHAR (50)   NULL,
    [SubChannel]     VARCHAR (50)   NULL,
    [Type]           VARCHAR (3)    NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);


GO
PRINT N'Creating [dbo].[Customer].[IX_Customer]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Customer]
    ON [dbo].[Customer]([CustomerName] ASC);


GO
PRINT N'Creating [dbo].[Employee]...';


GO
CREATE TABLE [dbo].[Employee] (
    [EmployeeID]   INT           NOT NULL,
    [EmployeeName] VARCHAR (150) NULL,
    [EmployeeType] VARCHAR (50)  NULL,
    [PostCode]     VARCHAR (50)  NULL,
    [State]        VARCHAR (50)  NULL,
    [TeamCode]     VARCHAR (50)  NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([EmployeeID] ASC)
);


GO
PRINT N'Creating [dbo].[Product]...';


GO
CREATE TABLE [dbo].[Product] (
    [ProductID]             INT              NULL,
    [Name]                  NVARCHAR (50)    NULL,
    [ProductNumber]         NVARCHAR (25)    NULL,
    [MakeFlag]              BIT              NULL,
    [FinishedGoodsFlag]     BIT              NULL,
    [Color]                 NVARCHAR (15)    NULL,
    [SafetyStockLevel]      SMALLINT         NULL,
    [ReorderPoint]          SMALLINT         NULL,
    [StandardCost]          MONEY            NULL,
    [ListPrice]             MONEY            NULL,
    [Size]                  NVARCHAR (5)     NULL,
    [SizeUnitMeasureCode]   NVARCHAR (3)     NULL,
    [WeightUnitMeasureCode] NVARCHAR (3)     NULL,
    [Weight]                NUMERIC (8, 2)   NULL,
    [DaysToManufacture]     INT              NULL,
    [ProductLine]           NVARCHAR (2)     NULL,
    [Class]                 NVARCHAR (2)     NULL,
    [Style]                 NVARCHAR (2)     NULL,
    [ProductSubcategoryID]  INT              NULL,
    [ProductModelID]        INT              NULL,
    [SellStartDate]         DATETIME         NULL,
    [SellEndDate]           DATETIME         NULL,
    [DiscontinuedDate]      DATETIME         NULL,
    [rowguid]               UNIQUEIDENTIFIER NULL,
    [ModifiedDate]          DATETIME         NULL
);


GO
PRINT N'Update complete.';


GO
