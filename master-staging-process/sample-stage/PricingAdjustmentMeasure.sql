﻿CREATE TABLE [dbo].[PricingAdjustmentMeasure](
	[PricingAdjustmentCode] [nvarchar](50) NOT NULL,
	[PricingAdjustmentName] [nvarchar](100) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL
)