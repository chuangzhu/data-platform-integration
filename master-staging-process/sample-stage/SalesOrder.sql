﻿CREATE TABLE [dbo].[SalesOrder]
(
	OrderCompany NVARCHAR(5) NULL,
	OrderNumber	NVARCHAR(50) NULL,
	OrderType NVARCHAR(50) NULL,
	LineID Decimal(6,0) NULL,
	BranchPlant NVARCHAR(50) NULL,
	CustomerId NVARCHAR(50) NULL,
	OrderDate DECIMAL(6,0) NULL,
	ShipmentDate DECIMAL(6,0) NULL,
	InvoiceDate DECIMAL(6,0) NULL,
	ProductId NVARCHAR(25) NULL,
	LineType NVARCHAR(2) NULL,
	UOM NVARCHAR(2) NULL,
	QuantityOrdered DECIMAL(15,0) NULL,
	QuantityShipped DECIMAL(15,0) NULL,
	QuantityBackordered DECIMAL (15,0) NULL,
	QuantityCancelled DECIMAL (15,0) NULL,
	InvoiceSales DECIMAL(15,0) NULL,
	ListPriceUnit DECIMAL(15,0) NULL,
	Unit DECIMAL(15,0) NULL,
	ListPrice DECIMAL(15,2) NULL,
	COGS DECIMAL(15,2) NULL,
	Rout NVARCHAR(3) NULL,
	ShipmentNo DECIMAL(8,0) NULL, 
    [updateDate] DATE NULL,
	            [CartonOrdered]                                    INT NULL,
            [CartonShipped]                                    INT NULL,
            [CartonCancelled]                                  INT NULL,
            [CartonBackordered]                             INT NULL,
            [UnitOrdered]                                        INT NULL,
            [UnitShipped]                                        INT NULL,
            [UnitCancelled]                                      INT NULL,
            [UnitBackordered]                                 INT NULL,
            [ValueOrdered]                                      DECIMAL(19, 4) NULL,
            [ValueShipped]                                     DECIMAL(19, 4) NULL,
            [ValueCancelled]                                   DECIMAL(19, 4) NULL,
            [ValueBackordered]                               DECIMAL(19, 4) NULL,
		[ProductClass] NCHAR(3) NULL,
	[SourceRegion]  NVARCHAR (50) NOT NULL,
	[SourceProvider] NVARCHAR (50) NOT NULL



)

GO

CREATE INDEX [IX_SalesOrder_Column] ON [dbo].[SalesOrder] (OrderNumber, CustomerId, OrderDate, ProductId, LineId)
