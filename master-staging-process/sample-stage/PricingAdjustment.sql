﻿CREATE TABLE [dbo].[PricingAdjustment](
	[InvoiceDateSK] [int] NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[BranchPlantCode] [nvarchar](50) NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[OrderType] [nvarchar](50) NOT NULL,
	[LineID] [decimal](6, 0) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[LineType] [nvarchar](20) NULL,
	[PricingAdjustmentCode] [nvarchar](50) NOT NULL,
	[Value] [decimal](19, 4) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL
)
