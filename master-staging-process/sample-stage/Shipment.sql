﻿CREATE TABLE [dbo].[Shipment]
(
	PromisedShipDate DECIMAL(6,0) NULL,
	PromisedDeliveryDate DECIMAL(6,0) NULL,
	ShipmentWeight DECIMAL(15,4) NULL,
	HoldReleaseDate NVARCHAR(2) NULL,
	HoldReleaseCode NVARCHAR(2) NULL,
	[SourceRegion]  NVARCHAR (50) NOT NULL,
	[SourceProvider] NVARCHAR (50) NOT NULL
)
