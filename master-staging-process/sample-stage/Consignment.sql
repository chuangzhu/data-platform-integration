﻿CREATE TABLE [dbo].[Consignment] (
    [ConsignmentNo]         VARCHAR (50)  NOT NULL,
    [ConsignmentItems]      INT			 NULL,
    [ItemCount]             INT           NULL,
	[Receiver]				VARCHAR(255) NULL,
    [PostCode]              VARCHAR (25)  NULL,
    [ServiceProvideDepot]   VARCHAR (50)  NULL,
    [Status]                VARCHAR (50)  NULL,
    [StatusAvailabilityDate] DATETIME      NULL,
    [StatusCaptureDate]     DATETIME      NULL,
    [Suburb]                VARCHAR (50) NULL,
	[FileName]				VARCHAR(50) NULL,
	[SourceRegion]  NVARCHAR (50) NOT NULL,
	[SourceProvider] NVARCHAR (50) NOT NULL
	

);

