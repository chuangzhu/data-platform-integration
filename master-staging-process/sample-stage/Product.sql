﻿CREATE TABLE [dbo].[Product] (
	[ProductSKU]	NVARCHAR(25) NOT NULL,
	[ProductName] NVARCHAR(30) NULL,
	[ProductFamilyCode] NVARCHAR(3) NULL,
	[ProductFamily]  NVARCHAR(30) NULL, 
	[ProductSegmentCode] NVARCHAR(3) NULL,
	[ProductSegment]  NVARCHAR(30) NULL,
	[ProductConditionCode] NVARCHAR(3) NULL,
	[ProductCondition]  NVARCHAR(30) NULL,
	[ProductGroupCode] NVARCHAR(3) NULL,
	[ProductGroup]  NVARCHAR(30) NULL,
	[CommodityClass] NVARCHAR(3) NULL,
	[CommoditySubClass] NVARCHAR(3) NULL,
	[StockType]		NVARCHAR(1) NULL,
	[ShopperSegment] NVARCHAR(25) NULL,
	[ConsumerGroup] NVARCHAR(25) NULL,
	[BusinessPriority] NVARCHAR(25) NULL,
	[ProductClassCode] NVARCHAR(3) NULL,
	[ProductClass]  NVARCHAR(30) NULL,
	[SourceRegion]  NVARCHAR (50) NOT NULL,
	[SourceProvider] NVARCHAR (50) NOT NULL

);


GO

CREATE INDEX [IX_Product_Column] ON [dbo].[Product] (ProductSKU)
