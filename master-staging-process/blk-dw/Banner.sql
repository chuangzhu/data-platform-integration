﻿CREATE TABLE [dbo].[Banner] (
    [BannerCode]       VARCHAR (50)  NOT NULL,
    [BannerName]       VARCHAR (100) NULL,
    [ParentBannerCode] VARCHAR (50)  NULL,
    [ParentBannerName] VARCHAR (100) NULL,
    CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED ([BannerCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Banner]
    ON [dbo].[Banner]([BannerName] ASC);

