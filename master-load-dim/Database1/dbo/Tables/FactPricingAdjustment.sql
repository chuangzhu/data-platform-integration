﻿CREATE TABLE [dbo].[FactPricingAdjustment](
	[TransSK] [int] NOT NULL,
	[InvoiceDateSK] [int] NOT NULL,
	[CustomerSK] [int] NOT NULL,
	[ProductSK] [int] NOT NULL,
	[BranchPlantSK] [int] NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[OrderType] [nvarchar](50) NOT NULL,
	[LineID] [decimal](6, 0) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[LineType] [nvarchar](20) NULL,
	[PricingAdjustmentSK] [int] NOT NULL,
	[Category] [nvarchar](100) NULL,
	[Value] [decimal](19, 4) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
 CONSTRAINT [PK_FactPricingAdj] PRIMARY KEY CLUSTERED 
(
	[InvoiceDateSK] ASC,
	[CustomerSK] ASC,
	[BranchPlantSK] ASC,
	[ProductSK] ASC,
	[OrderNo] ASC,
	[OrderType] ASC,
	[LineID] ASC,
	[PricingAdjustmentSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
ALTER TABLE [dbo].[FactPricingAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_FactPricingAdj_DimBranchPlant] FOREIGN KEY([BranchPlantSK])
REFERENCES [dbo].[DimBranchPlant] ([BranchPlantSK])
GO

ALTER TABLE [dbo].[FactPricingAdjustment] CHECK CONSTRAINT [FK_FactPricingAdj_DimBranchPlant]
GO
ALTER TABLE [dbo].[FactPricingAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_FactPricingAdj_DimCustomer] FOREIGN KEY([CustomerSK])
REFERENCES [dbo].[DimCustomer] ([CustomerSK])
GO

ALTER TABLE [dbo].[FactPricingAdjustment] CHECK CONSTRAINT [FK_FactPricingAdj_DimCustomer]
GO
ALTER TABLE [dbo].[FactPricingAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_FactPricingAdj_DimDate] FOREIGN KEY([InvoiceDateSK])
REFERENCES [dbo].[DimDate] ([DateSK])
GO

ALTER TABLE [dbo].[FactPricingAdjustment] CHECK CONSTRAINT [FK_FactPricingAdj_DimDate]
GO


ALTER TABLE [dbo].[FactPricingAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_FactPricingAdj_DimProduct] FOREIGN KEY([ProductSK])
REFERENCES [dbo].[DimProduct] ([ProductSK])
GO

ALTER TABLE [dbo].[FactPricingAdjustment] CHECK CONSTRAINT [FK_FactPricingAdj_DimProduct]