﻿CREATE TABLE [dbo].[DimGeography](
	[GeographyId] [int] IDENTITY(1,1) NOT NULL,
	[PostCode] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[CountryName] [nvarchar](50) NULL,
	[PopulationCount] [int] NULL,
	[Area sqkm] [decimal](10, 2) NULL,
	[Latitude] [decimal](10, 6) NULL,
	[Longitude] [decimal](10, 6) NULL,
	[CountryFlagURL] [varchar](500) NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [PK_DimGeography] PRIMARY KEY CLUSTERED 
(
	[GeographyId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)