﻿CREATE TABLE [dbo].[DimCompanyBrand](
	[CompanyBrandSK] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[CompanyBrandName] [nvarchar](100) NOT NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [PK_DimCompanyBrand] PRIMARY KEY CLUSTERED 
(
	[CompanyBrandSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)