﻿CREATE TABLE [dbo].[FactBudget](
	[ForecastSK] [int] NOT NULL,
	[DateSK] [int] NOT NULL,
	[CustomerSK] [int] NOT NULL,
	[ProductSK] [int] NOT NULL,
	[InboundEmployeeSK] [int] NOT NULL,
	[OutBoundEmployeeSK] [int] NOT NULL,
	[UnitBudget] [decimal](19, 4) NULL,
	[ListPriceBudget] [decimal](19, 4) NULL,
	[InvoicedSalesBudget] [decimal](19, 4) NULL,
	[COGSBudget] [decimal](19, 4) NULL,
	[GrossMarginBudget] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FactBudget] PRIMARY KEY CLUSTERED 
(
	[DateSK] ASC,
	[CustomerSK] ASC,
	[ProductSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO
ALTER TABLE [dbo].[FactBudget]  WITH CHECK ADD  CONSTRAINT [FK_FactBudget_DimCustomer] FOREIGN KEY([CustomerSK])
REFERENCES [dbo].[DimCustomer] ([CustomerSK])
GO

ALTER TABLE [dbo].[FactBudget] CHECK CONSTRAINT [FK_FactBudget_DimCustomer]
GO
ALTER TABLE [dbo].[FactBudget]  WITH CHECK ADD  CONSTRAINT [FK_FactBudget_DimDate] FOREIGN KEY([DateSK])
REFERENCES [dbo].[DimDate] ([DateSK])
GO

ALTER TABLE [dbo].[FactBudget] CHECK CONSTRAINT [FK_FactBudget_DimDate]
GO
ALTER TABLE [dbo].[FactBudget]  WITH CHECK ADD  CONSTRAINT [FK_FactBudget_DimProduct] FOREIGN KEY([ProductSK])
REFERENCES [dbo].[DimProduct] ([ProductSK])
GO

ALTER TABLE [dbo].[FactBudget] CHECK CONSTRAINT [FK_FactBudget_DimProduct]
GO
ALTER TABLE [dbo].[FactBudget]  WITH CHECK ADD  CONSTRAINT [FK_FactBudget_InboundEmployee] FOREIGN KEY([InboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactBudget] CHECK CONSTRAINT [FK_FactBudget_InboundEmployee]
GO
ALTER TABLE [dbo].[FactBudget]  WITH CHECK ADD  CONSTRAINT [FK_FactBudget_OutboundEmployee] FOREIGN KEY([OutBoundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactBudget] CHECK CONSTRAINT [FK_FactBudget_OutboundEmployee]