﻿
CREATE TABLE [dbo].[FactOrder](
	[InvoiceDateSK] [int] NOT NULL,
	[CustomerSK] [int] NOT NULL,
	[ProductSK] [int] NOT NULL,
	[BranchPlantSK] [int] NOT NULL,
	[InboundEmployeeSK] [int] NOT NULL,
	[OutboundEmployeeSK] [int] NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[OrderType] [nvarchar](50) NOT NULL,
	[LineID] [decimal](6, 0) NOT NULL,
	[Unit] [int] NULL,
	[COG] [decimal](19, 4) NULL,
	[InvoiceSale] [decimal](19, 4) NULL,
	[ListPrice] [decimal](19, 4) NULL,
	[GrossMargin] [decimal](19, 4) NULL,
	[CurrencyPair] [nvarchar](10) NOT NULL,
	[ExchangeRate] [decimal](19, 4) NOT NULL,
	[ReturnCount] [int] NULL,
	[ReturnValue] [decimal](19, 4) NULL,
	[OID] [decimal](19, 4) NULL,
	[ManualRebate] [decimal](19, 4) NULL,
	[NetSales] [decimal](19, 4) NULL,
	[TradeSpend] [decimal](19, 4) NULL,
	[TransSK] [int] NULL,
	[ShipmentNo] [nvarchar](50) NULL,
	[Backordered/Cancelled] [nvarchar](20) NULL,
	[OrderDateSK] [int] NOT NULL,
	[Rout] [nvarchar](250) NULL,
	[ShipmentDateSK] [int] NULL,
	[BackorderReleaseDateSK] [int] NULL,
	[LineType] [nvarchar](20) NULL,
	[QuantityOrdered] [int] NULL,
	[QuantityShipped] [int] NULL,
	[QuantityCancelled] [int] NULL,
	[QuantityBackordered] [int] NULL,
	[UOM] [nvarchar](20) NULL,
	[CartonOrdered] [int] NULL,
	[CartonShipped] [int] NULL,
	[CartonCancelled] [int] NULL,
	[CartonBackordered] [int] NULL,
	[UnitOrdered] [int] NULL,
	[UnitShipped] [int] NULL,
	[UnitCancelled] [int] NULL,
	[UnitBackordered] [int] NULL,
	[ValueOrdered] [decimal](19, 4) NULL,
	[ValueShipped] [decimal](19, 4) NULL,
	[ValueCancelled] [decimal](19, 4) NULL,
	[ValueBackordered] [decimal](19, 4) NULL,
	[OrderSK] [int] NULL,
	[Live] [bit] NULL,
	[HoldCode] [nvarchar](50) NULL,
	[EstimatedInvoicedSales] [decimal](19, 4) NULL,
	[LiveClassification] [nvarchar](50) NULL,
	[NextStatus] [nvarchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FactOrder] PRIMARY KEY CLUSTERED 
(
	[OrderDateSK] ASC,
	[CustomerSK] ASC,
	[BranchPlantSK] ASC,
	[ProductSK] ASC,
	[OrderNo] ASC,
	[OrderType] ASC,
	[LineID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_DimBranchPlant] FOREIGN KEY([BranchPlantSK])
REFERENCES [dbo].[DimBranchPlant] ([BranchPlantSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_DimBranchPlant]
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_DimCustomer] FOREIGN KEY([CustomerSK])
REFERENCES [dbo].[DimCustomer] ([CustomerSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_DimCustomer]
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_DimDate] FOREIGN KEY([OrderDateSK])
REFERENCES [dbo].[DimDate] ([DateSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_DimDate]
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_DimProduct] FOREIGN KEY([ProductSK])
REFERENCES [dbo].[DimProduct] ([ProductSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_DimProduct]
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_InboundEmployee] FOREIGN KEY([InboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_InboundEmployee]
GO

ALTER TABLE [dbo].[FactOrder]  WITH NOCHECK ADD  CONSTRAINT [FK_FactOrder_OutboundEmployee] FOREIGN KEY([OutboundEmployeeSK])
REFERENCES [dbo].[DimEmployee] ([EmployeeSK])
GO

ALTER TABLE [dbo].[FactOrder] CHECK CONSTRAINT [FK_FactOrder_OutboundEmployee]
GO