﻿
CREATE TABLE [dbo].[DimBanner](
	[BannerSK] [int] IDENTITY(1,1) NOT NULL,
	[BannerCode] [nvarchar](50) NULL,
	[BannerName] [nvarchar](100) NULL,
	[ParentBannerCode] [nvarchar](50) NULL,
	[ParentBannerName] [nvarchar](100) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [PK_DimBanner] PRIMARY KEY CLUSTERED 
(
	[BannerSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)