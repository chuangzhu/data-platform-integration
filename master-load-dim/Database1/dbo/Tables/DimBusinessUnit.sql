﻿
CREATE TABLE [dbo].[DimBusinessUnit](
	[BusinessUnitSK] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [nvarchar](100) NULL,
	[BusinessUnitName] [nvarchar](150) NULL,
	[BusinessUnitType] [nvarchar](50) NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [nvarchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
 CONSTRAINT [PK_DimBusinessUnit] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)