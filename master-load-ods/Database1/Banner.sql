﻿CREATE TABLE [dbo].[Banner](
	[BannerCode] [nvarchar](50) NOT NULL,
	[BannerName] [nvarchar](100) NULL,
	[ParentBannerCode] [nvarchar](50) NULL,
	[ParentBannerName] [nvarchar](100) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[BannerCode] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)