﻿CREATE TABLE [dbo].[PricingAdjustment](
	[InvoiceDateSK] [int] NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[BranchPlantCode] [nvarchar](50) NOT NULL,
	[OrderNo] [nvarchar](50) NOT NULL,
	[OrderType] [nvarchar](50) NOT NULL,
	[LineID] [decimal](6, 0) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[LineType] [nvarchar](20) NULL,
	[PricingAdjustmentCode] [nvarchar](50) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[Value] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PricingAdj] PRIMARY KEY CLUSTERED 
(
	[InvoiceDateSK] ASC,
	[CustomerID] ASC,
	[BranchPlantCode] ASC,
	[ProductSKU] ASC,
	[OrderNo] ASC,
	[OrderType] ASC,
	[LineID] ASC,
	[PricingAdjustmentCode] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)