﻿
CREATE TABLE [dbo].[Forecast](
	[ForecastSK] [int] NOT NULL,
	[DateSK] [int] NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[InboundEmployeeID] [nvarchar](50) NULL,
	[OutboundEmployeeID] [nvarchar](50) NULL,
	[UnitForecast] [int] NULL,
	[InvoicedSalesForecast] [decimal](19, 4) NULL,
	[COGForecast] [decimal](19, 4) NULL,
	[GrossMarginForecast] [decimal](19, 4) NULL,
	[ManualRebateForecast] [decimal](19, 4) NULL,
	[NetSalesForecast] [decimal](19, 4) NULL,
	[TradeSpend] [decimal](19, 4) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_Forecast] PRIMARY KEY CLUSTERED 
(
	[ForecastSK] ASC,
	[DateSK] ASC,
	[CustomerID] ASC,
	[ProductSKU] ASC,
	[SourceRegion] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[Forecast]  WITH NOCHECK ADD  CONSTRAINT [FK_Forecast_Customer] FOREIGN KEY([CustomerID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Customer] ([CustomerID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[Forecast] CHECK CONSTRAINT [FK_Forecast_Customer]
GO

ALTER TABLE [dbo].[Forecast]  WITH CHECK ADD  CONSTRAINT [FK_Forecast_Date] FOREIGN KEY([DateSK])
REFERENCES [dbo].[Date] ([DateSK])
GO

ALTER TABLE [dbo].[Forecast] CHECK CONSTRAINT [FK_Forecast_Date]
GO

ALTER TABLE [dbo].[Forecast]  WITH NOCHECK ADD  CONSTRAINT [FK_Forecast_InboundEmployee] FOREIGN KEY([InboundEmployeeID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Employee] ([EmployeeID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[Forecast] CHECK CONSTRAINT [FK_Forecast_InboundEmployee]
GO

ALTER TABLE [dbo].[Forecast]  WITH NOCHECK ADD  CONSTRAINT [FK_Forecast_OutboundEmployee] FOREIGN KEY([OutboundEmployeeID], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Employee] ([EmployeeID], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[Forecast] CHECK CONSTRAINT [FK_Forecast_OutboundEmployee]
GO

ALTER TABLE [dbo].[Forecast]  WITH NOCHECK ADD  CONSTRAINT [FK_Forecast_Product] FOREIGN KEY([ProductSKU], [SourceRegion], [SourceProvider])
REFERENCES [dbo].[Product] ([ProductSKU], [SourceRegion], [SourceProvider])
GO

ALTER TABLE [dbo].[Forecast] CHECK CONSTRAINT [FK_Forecast_Product]
GO