﻿CREATE TABLE [dbo].[CompanyBrand](
	[CompanyName] [nvarchar](50) NOT NULL,
	[CompanyBrandName] [nvarchar](100) NOT NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[InsertDate] DATETIME NULL, 
    [InsertAuditKey] INT NULL, 
    [UpdateDate] DATETIME NULL, 
    [UpdateAuditKey] INT NULL, 
 CONSTRAINT [PK_CompanyBrand] PRIMARY KEY CLUSTERED 
(
	[CompanyName] ASC,
	[CompanyBrandName] ASC,
	[SourceRegion] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)