﻿CREATE TABLE [dbo].[BusinessUnit](
	[BusinessUnitID] [nvarchar](100) NOT NULL,
	[BusinessUnitName] [nvarchar](150) NULL,
	[BusinessUnitType] [nvarchar](50) NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL
 
)