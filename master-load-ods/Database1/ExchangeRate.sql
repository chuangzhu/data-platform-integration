﻿CREATE TABLE [dbo].[ExchangeRate](
	[CurrencyPair] [varchar](10) NOT NULL,
	[DateSK] [int] NOT NULL,
	[ExchangeRate] [decimal](19, 4) NOT NULL,
	[ExchangeRateSource] [nvarchar](50) NOT NULL,
	[ExchangeRateType] [nvarchar](50) NOT NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ExchangeRate] PRIMARY KEY CLUSTERED 
(
	[CurrencyPair] ASC,
	[DateSK] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)
GO

ALTER TABLE [dbo].[ExchangeRate]  WITH NOCHECK ADD  CONSTRAINT [FK_FXRate_Date] FOREIGN KEY([DateSK])
REFERENCES [dbo].[Date] ([DateSK])
GO

ALTER TABLE [dbo].[ExchangeRate] CHECK CONSTRAINT [FK_FXRate_Date]
GO