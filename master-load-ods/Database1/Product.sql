﻿
CREATE TABLE [dbo].[Product](
	[ProductSKU] [nvarchar](100) NOT NULL,
	[ProductName] [nvarchar](255) NULL,
	[ProductFamily] [nvarchar](100) NULL,
	[ProductSegment] [nvarchar](100) NULL,
	[ProductCondition] [nvarchar](100) NULL,
	[ProductGroup] [nvarchar](100) NULL,
	[ProductClass] [nvarchar](100) NULL,
	[StockType] [nvarchar](50) NULL,
	[ProductStrategy] [nvarchar](50) NULL,
	[CommodityClass] [nvarchar](50) NULL,
	[CommoditySubClass] [nvarchar](50) NULL,
	[ShopperSegment] [nvarchar](100) NULL,
	[ConsumerGroup] [nvarchar](100) NULL,
	[BusinessPriority] [nvarchar](100) NULL,
	[ProductImageURL] [nvarchar](500) NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[CompanyBrandName] [nvarchar](100) NOT NULL,
	[MasterProductSKU] [nvarchar](100) NULL,
	[MasterProductName] [nvarchar](255) NULL,
	[MasterProductSourceRegion] [nvarchar](50) NULL,
	[MasterProductSourceProvider] [nvarchar](50) NULL,
	[CompetitorProductSKU] [nvarchar](100) NULL,
	[CompetitorProductName] [nvarchar](255) NULL,
	[CompetitorProductSourceRegion] [nvarchar](50) NULL,
	[CompetitorProductSourceProvider] [nvarchar](50) NULL,
	[AztecConsumerSegment] [nvarchar](50) NULL,
	[AztecShopperSegment] [nvarchar](50) NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL,
	[IsActive] [varchar](50) NULL,
	[InsertDate] [Datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [Datetime] NULL,
	[UpdateAuditKey] [int] NULL
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductSKU] ASC,
	[SourceRegion] ASC,
	[SourceProvider] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF)
)