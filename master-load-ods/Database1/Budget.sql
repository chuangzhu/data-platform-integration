﻿CREATE TABLE [dbo].[Budget](
	[ForecastSK] [int] NULL,
	[DateSK] [int] NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[ProductSKU] [nvarchar](100) NOT NULL,
	[InboundEmployeeID] [nvarchar](50) NULL,
	[OutboundEmployeeID] [nvarchar](50) NULL,
	[UnitBudget] [decimal](19, 4) NULL,
	[ListPriceBudget] [decimal](19, 4) NULL,
	[InvoicedSalesBudget] [decimal](19, 4) NULL,
	[COGSBudget] [decimal](19, 4) NULL,
	[GrossMarginBudget] [decimal](19, 4) NULL,
	[InsertDate] [datetime] NULL,
	[InsertAuditKey] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateAuditKey] [int] NULL,
	[SourceRegion] [nvarchar](50) NOT NULL,
	[SourceProvider] [nvarchar](50) NOT NULL
)

